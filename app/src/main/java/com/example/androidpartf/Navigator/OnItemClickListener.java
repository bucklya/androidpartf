package com.example.androidpartf.Navigator;

import com.example.androidpartf.network.model.Country;

public interface OnItemClickListener {
    void onItemClick(Country country);
}
