package com.example.androidpartf.Navigator;

public interface OnButtonClickListener {
    void onButtonClick(String inputName);
}
