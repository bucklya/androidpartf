package com.example.androidpartf;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.androidpartf.Navigator.OnButtonClickListener;
import com.example.androidpartf.Navigator.OnItemClickListener;
import com.example.androidpartf.fragments.InfoFragment;
import com.example.androidpartf.fragments.ListFragment;
import com.example.androidpartf.fragments.StartFragment;
import com.example.androidpartf.network.model.Country;

import java.util.List;

public class MainActivity extends AppCompatActivity implements OnButtonClickListener, OnItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ListFragment.newInstance("2"))
                .commit();
    }

    @Override
    public void onButtonClick(String inputName) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ListFragment.newInstance(inputName))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onItemClick(Country country) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, InfoFragment.newInstance(country))
                .addToBackStack(null)
                .commit();
    }
}
