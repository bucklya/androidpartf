package com.example.androidpartf.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.androidpartf.Navigator.OnItemClickListener;
import com.example.androidpartf.R;
import com.example.androidpartf.databinding.ItemCountryBinding;
import com.example.androidpartf.network.model.Country;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryHolder> {

    private Context context;
    private List<Country> list = new ArrayList<>();
    private OnItemClickListener listener;

    public CountryAdapter(Context context, OnItemClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CountryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemCountryBinding binding = ItemCountryBinding.inflate(LayoutInflater.from(context));
        return new CountryHolder(binding, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryHolder holder, int position) {
        Country country = list.get(position);
        holder.binding.nameCountry.setText(country.name);
        holder.binding.code.setText(country.alpha3Code);
//        Picasso.with(context)
//                .load(country.flag)
//                .error(R.drawable.error)
//                .noFade()
//                .into(holder.binding.flag);
        holder.binding.flag.setImageResource(R.drawable.error);
        holder.bind(country);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<Country> countries) {
        list.clear();
        list.addAll(countries);
        notifyDataSetChanged();
    }

    static class CountryHolder extends RecyclerView.ViewHolder {

        private ItemCountryBinding binding;
        private OnItemClickListener listener;

        public CountryHolder(ItemCountryBinding binding, OnItemClickListener listener) {
            super(binding.getRoot());
            this.binding = binding;
            this.listener = listener;
        }

        public void bind(Country country) {
            binding.item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(country);
                }
            });
        }
    }
}
