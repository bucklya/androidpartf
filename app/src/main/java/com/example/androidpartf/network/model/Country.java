package com.example.androidpartf.network.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Country implements Parcelable {
    public String name;
    public String alpha3Code;
    public String capital;
    public String region;
    public int population;
    public String flag;

    protected Country(Parcel in) {
        name = in.readString();
        alpha3Code = in.readString();
        capital = in.readString();
        region = in.readString();
        population = in.readInt();
        flag = in.readString();
    }

    public static final Creator<Country> CREATOR = new Creator<Country>() {
        @Override
        public Country createFromParcel(Parcel in) {
            return new Country(in);
        }

        @Override
        public Country[] newArray(int size) {
            return new Country[size];
        }
    };

    public String getName() {
        return name;
    }

    public int getPopulation() {
        return population;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(alpha3Code);
        dest.writeString(capital);
        dest.writeString(region);
        dest.writeInt(population);
        dest.writeString(flag);
    }
}
