package com.example.androidpartf.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.androidpartf.R;
import com.example.androidpartf.databinding.FragmentInfoBinding;
import com.example.androidpartf.network.model.Country;

public class InfoFragment extends Fragment {

    private static final String ARG = "country";

    private Country clickCountry;

    private FragmentInfoBinding binding;

    public static InfoFragment newInstance(Country country) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG, country);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            clickCountry = getArguments().getParcelable(ARG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentInfoBinding.inflate(inflater, container, false);
        View v = binding.getRoot();

        binding.infoName.setText(clickCountry.name);
        binding.infoCode.setText(clickCountry.alpha3Code);
        binding.infoCapital.setText(clickCountry.capital);
        binding.infoPopulation.setText(Integer.toString(clickCountry.population));
        binding.infoRegion.setText(clickCountry.region);

        return v;
    }
}
