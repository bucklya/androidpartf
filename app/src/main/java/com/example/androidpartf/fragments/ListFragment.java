package com.example.androidpartf.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.androidpartf.Navigator.OnItemClickListener;
import com.example.androidpartf.adapter.CountryAdapter;
import com.example.androidpartf.databinding.FragmentListBinding;
import com.example.androidpartf.network.ApiService;
import com.example.androidpartf.network.model.Country;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

public class ListFragment extends Fragment {

    private static final String ARG_NAME = "param1";

    private FragmentListBinding binding;
    private String inputName;
    private CountryAdapter adapter;
    private Disposable disposable;
    private OnItemClickListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (OnItemClickListener) context;
    }

    public static ListFragment newInstance(String param1) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_NAME, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            inputName = getArguments().getString(ARG_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentListBinding.inflate(inflater, container, false);
        View v = binding.getRoot();

        adapter = new CountryAdapter(v.getContext(), listener);
        binding.listCountry.setAdapter(adapter);

        disposable = ApiService.getData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
//                .cast(Country.class)
//                .filter(x -> x.name.toUpperCase().contains(inputName.toUpperCase()))
//                .toList()
                .subscribe(this::showCountries, this::showError);

        return v;
    }

    public void showCountries(List<Country> countries) {
        adapter.updateList(countries);
    }

    public void showError(Throwable t) {
        Toast.makeText(binding.getRoot().getContext(), "Error", Toast.LENGTH_SHORT).show();
    }
}
